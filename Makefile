all: students

students: main.o 
	g++ main.o -o students

main.o: main.cpp
	g++ -c main.cpp

clean:
	rm -rf *o students