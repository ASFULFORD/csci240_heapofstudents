//Date.cpp
using namespace std;
#include "Address.h"
#include<sstream>

Address::Address()
{

}
Address::Address(string line1, string line2, string city, string state, string zip)
{
    Address::line1 = line1;
    Address::line2 = line2;
    Address::city = city;
    Address::state = state;
    Address::zip = zip;
}
void Address::setLine1(string line1)
{
    Address::line1 = line1;
}
void Address::setLine2(string line2)
{
    Address::line2 = line2;
}
void Address::setCity(string city)
{
    Address::city = city;
}
void Address::setState(string state)
{
    Address::state = state;
}
void Address::setZip(string zip)
{
    Address::zip = zip;
}
string Address::getAddress()
{
    stringstream ss;
    ss << line1 << ", " << line2 << ", " << city << ", " << state << ", " << zip;
    return ss.str();
}
