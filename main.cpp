//main.cpp
using namespace std;
#include<fstream>
#include<iostream>
#include<string>
#include<sstream>
#include "Student.cpp"
#define SIZE 51

Student *student = new Student[SIZE];

void populateArray();
void printArray();
void printFullName();
void sortAlphabetically();
void mySwap(Student* a, Student* b);

int main()
{
    populateArray();
    printArray();
    printFullName();
    sortAlphabetically();
    printArray();
    printFullName();
    delete[] student;
    return 0;
}
void populateArray()
{
    int i;
    string fName;
    string lName;
    string gpa;
    string credits;
    string month;
    string day;
    string year;
    string line1;
    string line2;
    string city;
    string state;
    string zip;
    string line;

    string temp;

    Date dob;
    Date gradDate;
    Address address;

    ifstream inFile;                                //inputting file
    inFile.open("data.dat");

    while (!inFile.eof())
    {
        for(i = 0; i < SIZE; i++)
        {
            getline(inFile, temp);                  //grabbing each line
            istringstream ss(temp);

            getline(ss, lName, ',');                //grabbing an element from the line
            student[i].setLname(lName);             //setting that element to proper property

            getline(ss, fName, ',');                //rinse and repeat
            student[i].setFname(fName);

            getline(ss, line1, ',');                //for the address, an Address object needed
            address.setLine1(line1);                //to be created to pass in all parameters
            getline(ss, line2, ',');
            address.setLine2(line2);
            getline(ss, city, ',');
            address.setCity(city);
            getline(ss, state, ',');
            address.setState(state);
            getline(ss, zip, ',');
            address.setZip(zip);
            student[i].setAddress(address);         //once the Address object was created, that
                                                    //object is passed into the setAddress function.
            getline(ss, month, '/');
            dob.setMonth(month);                    //same situation for dates.  a Date object was
            getline(ss, day, '/');                  //created first
            dob.setDay(day);
            getline(ss, year, ',');
            dob.setYear(year);
            student[i].setDob(dob);                 //and passed into the setDate function.

            getline(ss, month, '/');
            gradDate.setMonth(month);
            getline(ss, day, '/');
            gradDate.setDay(day);
            getline(ss, year, ',');
            gradDate.setYear(year);
            student[i].setGradDate(gradDate);

            getline(ss, gpa, ',');
            student[i].setGpa(gpa);

            getline(ss, credits, ',');
            student[i].setCredits(credits);
        }
    }
    inFile.close();
}
void printArray()
{
    int i;

    for(i = 0; i < SIZE; i++)
    {
        cout << student[i].getStudent() << endl;
    }
}
void printFullName()
{
    int i;

    for(i = 0; i < SIZE; i++)
    {
        cout << student[i].getLname() << ", " << student[i].getFname() << endl;
    }
}
void sortAlphabetically()
{
    int i, j;

    for(i = 0; i < (SIZE - 1); i++)
    {
        for(j = 0; j < (SIZE - i - 1); j++)
        {
            if(student[j].getLname() > student[j+1].getLname())
            {
                mySwap(student + j, student + (j +1));
            }
        }
    }
}
void mySwap(Student* a, Student* b)
{
    Student temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

