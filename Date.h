//Date.h
using namespace std;
#ifndef DATE_H_EXISTS
#define DATE_H_EXISTS

class Date{
    private:
        string day;
        string month;
        string year;
    public:
        Date();
        Date(string day, string month, string year);
        void setDay(string day);
        void setMonth(string month);
        void setYear(string year);
        string getDate();
};
#endif
