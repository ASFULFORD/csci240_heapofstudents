//Student.h
using namespace std;
#ifndef STUDENT_H_EXISTS
#define STUDENT_H_EXISTS
#include "Date.cpp"
#include "Address.cpp"

class Student{
    private:
        string fName;
        string lName;
        string gpa;
        string credits;
        Date gradDate;
        Date dob;
        Address address;
    public:
        Student();
        Student(string fName, string lName, string gpa, string credits, Date gradDate, Date dob, Address address);
        void setFname(string fName);
        void setLname(string lName);
        void setGpa(string gpa);
        void setCredits(string credits);
        void setGradDate(Date gradDate);
        void setDob(Date dob);
        void setAddress(Address address);
        string getFname();
        string getLname();
        string getStudent();
};
#endif
