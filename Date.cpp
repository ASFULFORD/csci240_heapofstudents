//Date.cpp
using namespace std;
#include "Date.h"
#include<sstream>

Date::Date()
{

}
Date::Date(string day, string month, string year)
{
    Date::day = day;
    Date::month = month;
    Date::year = year;
}
void Date::setDay(string day)
{
    Date::day = day;
}
void Date::setMonth(string month)
{
    Date::month = month;
}
void Date::setYear(string year)
{
    Date::year = year;
}
string Date::getDate()
{
    stringstream ss;
    ss << month << "/" << day << "/" << year;
    return ss.str();
}
