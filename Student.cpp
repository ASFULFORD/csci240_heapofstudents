//Student.cpp
using namespace std;
#include<sstream>
#include "Student.h"
#include "Date.h"
#include "Address.h"

Student::Student()
{
}
Student::Student(string fName, string lName, string gpa, string credits, Date gradDate, Date dob, Address address)
{
    Student::fName = fName;
    Student::lName = lName;
    Student::gpa = gpa;
    Student::credits = credits;
    Student::gradDate = gradDate;
    Student::dob = dob;
    Student::address = address;
}
void Student::setFname(string fName)
{
    Student::fName = fName;
}
void Student::setLname(string lName)
{
    Student::lName = lName;
}
void Student::setGpa(string gpa)
{
    Student::gpa = gpa;
}
void Student::setCredits(string credits)
{
    Student::credits = credits;
}
void Student::setGradDate(Date gradDate)
{
    Student::gradDate = gradDate;
}
void Student::setDob(Date dob)
{
    Student::dob = dob;
}
void Student::setAddress(Address address)
{
    Student::address = address;
}
string Student::getFname()
{
    stringstream ss;
    ss << fName;
    return ss.str();
}
string Student::getLname()
{
    stringstream ss;
    ss << lName;
    return ss.str();
}
string Student::getStudent()
{
    stringstream ss;
    ss << lName << ", " << fName << ", " << address.getAddress() << ", " << dob.getDate() << ", " << gradDate.getDate() << ", " << gpa << ", " << credits << endl;
    return ss.str();
}
