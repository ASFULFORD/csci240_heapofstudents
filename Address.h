//Address.h
using namespace std;
#ifndef ADDRESS_H_EXISTS
#define ADDRESS_H_EXISTS

class Address{
    private:
        string line1;
        string line2;
        string city;
        string state;
        string zip;
    public:
        Address();
        Address(string line1, string line2, string city, string state, string zip);
        void setLine1(string line1);
        void setLine2(string line2);
        void setCity(string city);
        void setState(string state);
        void setZip(string zip);
        string getAddress();
};
#endif
